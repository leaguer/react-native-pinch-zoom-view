import React, { Component } from "react";
import PropTypes from "prop-types";
import { View, StyleSheet, PanResponder } from "react-native";

export default class PinchZoomView extends Component {
	static propTypes = {
		...View.propTypes,
		scalable: PropTypes.bool
	};

	static defaultProps = {
		minScale: 1,
		maxScale: Number.MAX_VALUE,
		offsetX: 0,
		offsetY: 0,
		scalable: true,
		scale: 1,
		onStart: noop,
		onEnd: noop
	};

	constructor(props) {
		super(props);

		this.state = {
			lastScale: clamp(props.scale, props.minScale, props.maxScale),
			minScale: props.minScale,
			maxScale: props.maxScale,
			lastMovePinch: false,
			lastX: props.offsetX,
			lastY: props.offsetY,
			offsetX: props.offsetX,
			offsetY: props.offsetY,
			onStart: props.onStart,
			onEnd: props.onEnd,
			scalable: props.scalable,
			scale: props.scale
		};

		this.distant = 150;
	}

	componentWillMount() {
		this.gestureHandlers = PanResponder.create({
			onStartShouldSetPanResponder: this
				._handleStartShouldSetPanResponder,
			onMoveShouldSetPanResponder: this._handleMoveShouldSetPanResponder,
			onPanResponderGrant: this._handlePanResponderGrant,
			onPanResponderMove: this._handlePanResponderMove,
			onPanResponderRelease: this._handlePanResponderEnd,
			onPanResponderTerminationRequest: evt => true,
			onShouldBlockNativeResponder: evt => false
		});
	}

	_handleStartShouldSetPanResponder = (e, gestureState) => {
		// don't respond to single touch to avoid shielding click on child components
		return false;
	};

	_handleMoveShouldSetPanResponder = (e, gestureState) => {
		return (
			this.props.scalable &&
			(Math.abs(gestureState.dx) > 2 ||
				Math.abs(gestureState.dy) > 2 ||
				gestureState.numberActiveTouches === 2)
		);
	};

	_handlePanResponderGrant = (e, gestureState) => {
		this.state.onStart();

		if (gestureState.numberActiveTouches === 2) {
			let dx = Math.abs(
				e.nativeEvent.touches[0].pageX - e.nativeEvent.touches[1].pageX
			);
			let dy = Math.abs(
				e.nativeEvent.touches[0].pageY - e.nativeEvent.touches[1].pageY
			);
			let distant = Math.sqrt(dx * dx + dy * dy);
			this.distant = distant;
		}
	};

	_handlePanResponderEnd = (e, gestureState) => {
		this.setState({
			lastX: this.state.offsetX,
			lastY: this.state.offsetY,
			lastScale: this.state.scale
		});

		this.state.onEnd();
	};

	_handlePanResponderMove = (e, gestureState) => {
		// zoom
		if (gestureState.numberActiveTouches === 2) {
			let dx = Math.abs(
				e.nativeEvent.touches[0].pageX - e.nativeEvent.touches[1].pageX
			);
			let dy = Math.abs(
				e.nativeEvent.touches[0].pageY - e.nativeEvent.touches[1].pageY
			);
			let distant = Math.sqrt(dx * dx + dy * dy);
			let scale = clamp(
				(distant / this.distant) * this.state.lastScale,
				this.state.minScale,
				this.state.maxScale
			);
			this.setState({ scale, lastMovePinch: true });
		}
		// translate
		else if (gestureState.numberActiveTouches === 1) {
			if (this.state.lastMovePinch) {
				gestureState.dx = 0;
				gestureState.dy = 0;
			}
			let offsetX = this.state.lastX + gestureState.dx / this.state.scale;
			let offsetY = this.state.lastY + gestureState.dy / this.state.scale;
			this.setState({ offsetX, offsetY, lastMovePinch: false });
		}
	};

	render() {
		return (
			<View
				{...this.gestureHandlers.panHandlers}
				style={[
					styles.container,
					this.props.style,
					{
						transform: [
							{ scaleX: this.state.scale },
							{ scaleY: this.state.scale },
							{ translateX: this.state.offsetX },
							{ translateY: this.state.offsetY }
						]
					}
				]}
			>
				{this.props.children}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center"
	}
});

function clamp(number, minimum, maximum) {
	return Math.min(Math.max(number, minimum), maximum);
}

function noop() {}
